// list of modules to deploy
module "moduloec2" {
  name="${var.name}"
  numero="${var.numero}"
  source="./tfmodules/ec2"
  ami_name="${var.ami_name}"
  ami="${var.ami}"
  type="${var.type}"
  key="${var.mikey}"
  volsize="${var.misize}"
  voltype="gp2"
  subnet="${var.subnet}"
  vpc_id="${var.vpc_id}"
  
  miregion="${var.regionaws}"
  miprofile="${var.profileaws}"

  service="${var.service}"
  versio="${var.version}"
  enviroment="${var.enviroment}"
}

