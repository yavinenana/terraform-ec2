output "ec2_id" {
  value = "${module.moduloec2.ec2_id}"
}
output "ec2_private" {
  value = "${module.moduloec2.ec2_private}"
}
output "ec2_security_group" {
  value = "${module.moduloec2.ec2_security_group}"
}

//output "ec2_elip" {
//  value = "${module.moduloec2.aws_elasticip}"
//}
