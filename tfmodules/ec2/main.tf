// RESOURCE AWS INSTANCE
/*  roles   = ["${aws_iam_role.s3_role.name}"] */
resource "aws_instance" "ec2_resource" {
  ami           = "${data.aws_ami.miami.id}"
//  ami		= "ami-059eeca93cf09eebd"
//  ami		= "${var.ami}"
  instance_type = "${var.type}"
  iam_instance_profile = "${aws_iam_instance_profile.test_profile.name}"
  key_name      = "${var.key}"
  count         = "${var.numero}"
  subnet_id     = "${var.subnet}"
  vpc_security_group_ids = ["${aws_security_group.sg_resource.id}"]
  root_block_device{
    volume_size = "${var.volsize}"
    volume_type = "${var.voltype}"
    delete_on_termination = "true"
  }
  tags {
    Name = "${var.service}-${var.versio}-${var.enviroment}-${var.name}-${count.index+1}"
  }
}

// RESOURCE SECURITY GROUP
resource "aws_security_group" "sg_resource" {
 name        = "sg_${var.enviroment}-${var.name}"
 description = "postgres-inbound && all-outbound"
// vpc_id      = "vpc-0e2f736dee853f8b0"
 vpc_id	     = "${var.vpc_id}"
 ingress {
   from_port   = 22
   to_port     = 22
   protocol    = "tcp"
   //security_groups = ["${var.security_group_access}"]
   cidr_blocks = ["0.0.0.0/0"]
 }
 ingress {
   from_port   = 5432
   to_port     = 5432
   protocol    = "tcp"
   cidr_blocks = ["0.0.0.0/0"]
 }
 egress {
   from_port   = 0
   to_port     = 0
   protocol    = "-1"
   cidr_blocks = ["0.0.0.0/0"]
 }
 tags {
    Name = "sg-${var.name}"
 }
}

// OUTPUT FROM TERRAFORM STATE  
output "ami_ids" {
  value = "${data.aws_ami.miami.id}"
}
output "instance_ips" {
  value = ["${aws_instance.ec2_resource.*.private_ip}"]
}
output "instance_type" {
  value = ["${aws_instance.ec2_resource.*.instance_type}"]
}
output "security_groups" {
  value = ["${aws_security_group.sg_resource.*.description}"]
  value = ["${aws_security_group.sg_resource.*.name}"]
}

