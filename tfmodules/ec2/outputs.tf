output "ec2_id" {
  value = "${aws_instance.ec2_resource.*.id}"
}
output "ec2_private" {
  value = "${aws_instance.ec2_resource.*.private_ip}"
}
output "ec2_security_group" {
  value = "${aws_security_group.sg_resource.id}"
}


//output "aws_elasticip"{
//  value = "${aws_eip.elasticip.id}"
//  value = "${aws_eip.elasticip.public_ip}"
//  value = "${aws_eip.elasticip.private_ip}"
//}
