data "aws_ami" "miami" {
  most_recent = true

  owners = ["self"]
  tags = {
    Name   = "miami2019"
    Tested = "true"
  }

  filter {
    name   = "name"
    values = ["${var.ami_name}"]
//    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}
