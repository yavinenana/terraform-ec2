// RESOURCE IAM ROLE
resource "aws_iam_role" "ec2_s3_access_role" {
  name               = "s3-role-${var.name}"
  assume_role_policy = "${file("assumerolepolicy.json")}"
  description = "role s3 full - * regions"
}

// RESOURCE IAM POLICY
resource "aws_iam_policy" "policy" {
  name        = "test-policy-${var.name}"
  description = "a test policy"
  policy      = "${file("policys3bucket.json")}"
}

// RESOURCE IAM POLICY ATTACHMENT
resource "aws_iam_policy_attachment" "test-attach" {
  name        = "test-attachment-${var.name}"
  roles       = ["${aws_iam_role.ec2_s3_access_role.name}"]
  policy_arn  = "${aws_iam_policy.policy.arn}"
}

// RESOURCE IAM INSTANCE PROFILE
resource "aws_iam_instance_profile" "test_profile" {
  name        = "test_profile-${var.name}" 
  roles       = ["${aws_iam_role.ec2_s3_access_role.name}"] 
}
