// ------------------terraform.tfvars to server_dbms
// ------------------postgresql_9.6
name	= "tf.dbms"
numero  = 1
misize = 20
ami_name = "ami-postgresql9.6-subnet"
ami     = "ami-0581bec9885c83427"
type    = "t3.micro"
subnet  = "subnet-039cbd74984023e5a"
vpc_id  = "vpc-0e2f736dee853f8b0"
mikey   = "2019_db"
regionaws = "us-east-1"
profileaws = "sysadmin-us-east-1"
// -------nomenclatura
service = "postgresql"
version = "9.6"
enviroment = "test"
